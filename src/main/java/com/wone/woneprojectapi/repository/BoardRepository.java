package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
}
