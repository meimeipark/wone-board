package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card, Long> {
}
