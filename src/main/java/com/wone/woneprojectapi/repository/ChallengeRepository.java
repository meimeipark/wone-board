package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Challenge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChallengeRepository extends JpaRepository<Challenge, Long> {
}
