package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Calendar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CalendarRepository extends JpaRepository<Calendar, Long> {
}
