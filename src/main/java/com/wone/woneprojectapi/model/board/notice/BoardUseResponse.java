package com.wone.woneprojectapi.model.board.notice;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardUseResponse {
    private Long id;
    private String boardType;
    private String boardTitle;
    private String boardContent;
}
