package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FaqUserChangeRequest {
    //수정 날짜 추가 시 날짜 필요
    private LocalDate askCreateDate;
    private String askContent;
    private Boolean publicType;
    private Short askPassword;
}
