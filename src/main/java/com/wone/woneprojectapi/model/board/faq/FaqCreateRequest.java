package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FaqCreateRequest {
    private LocalDate askCreateDate;
    private String askContent;
    private String comment;
    private Boolean publicType;
    private Short askPassword;
}
