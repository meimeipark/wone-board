package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FaqAdminItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private LocalDate askCreateDate;
    private String askContent;
    private String publicType;
    private String comment;
}
