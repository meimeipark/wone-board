package com.wone.woneprojectapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoneProjectApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoneProjectApiApplication.class, args);
    }

}
