package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Board;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.board.faq.FaqResponse;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard (Member member, BoardCreateRequest request){
        Board addData = new Board();
        addData.setMember(member);
        addData.setBoardType(request.getBoardType());
        addData.setBoardCreateData(LocalDate.now());
        addData.setBoardTitle(request.getBoardTitle());
        addData.setBoardContent(request.getBoardContent());

        boardRepository.save(addData);
    }

    //notice만 list 되도록 수정
    public List<BoardNoticeItem> getNoticeBoards(){
        List<Board> originList = boardRepository.findAll();

        List<BoardNoticeItem> result = new LinkedList<>();

        for(Board board: originList){
           BoardNoticeItem addItem = new BoardNoticeItem();
           addItem.setId(board.getId());
           addItem.setBoardType(board.getBoardType().getBoardType());
           addItem.setBoardCreateData(board.getBoardCreateData());
           addItem.setBoardTitle(board.getBoardTitle());
           addItem.setBoardContent(board.getBoardContent());

           result.add(addItem);
        }
        return result;
    }

    //use만 list되도록 수정
    public List<BoardUseItem> getUseBoards(){
        List<Board> originList = boardRepository.findAll();

        List<BoardUseItem> result = new LinkedList<>();
        for(Board board: originList){
            BoardUseItem addItem = new BoardUseItem();
            addItem.setId(board.getId());
            addItem.setBoardType(board.getBoardType().getBoardType());
            addItem.setBoardTitle(board.getBoardTitle());
            addItem.setBoardContent(board.getBoardContent());

            result.add(addItem);
        }
        return result;
    }

    public BoardNoticeResponse getNoticeBoard(long  id){
        Board originData = boardRepository.findById(id).orElseThrow();

        BoardNoticeResponse response = new BoardNoticeResponse();
        response.setId(originData.getId());
        response.setBoardType(originData.getBoardType().getBoardType());
        response.setBoardCreateData(originData.getBoardCreateData());
        response.setBoardTitle(originData.getBoardTitle());
        response.setBoardContent(originData.getBoardContent());

        return response;
    }

    public BoardUseResponse getUseBoard(long  id){
        Board originData = boardRepository.findById(id).orElseThrow();

        BoardUseResponse response = new BoardUseResponse();
        response.setId(originData.getId());
        response.setBoardType(originData.getBoardType().getBoardType());
        response.setBoardTitle(originData.getBoardTitle());
        response.setBoardContent(originData.getBoardContent());

        return response;
    }

    public void putBoard(long id, BoardContentChangeRequest request){
        Board originData = boardRepository.findById(id).orElseThrow();

        originData.setBoardType(request.getBoardType());
        originData.setBoardTitle(request.getBoardTitle());
        originData.setBoardContent(request.getBoardContent());

        boardRepository.save(originData);
    }

    public void delBoard(long id){
        boardRepository.deleteById(id);
    }
}
