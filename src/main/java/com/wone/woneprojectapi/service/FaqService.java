package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Faq;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.board.faq.*;
import com.wone.woneprojectapi.repository.FaqRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FaqService {
    private final FaqRepository faqRepository;

    public void setFaq (Member member,FaqCreateRequest request){
        Faq addData = new Faq();
        addData.setMember(member);
        addData.setAskCreateDate(LocalDate.now());
        addData.setAskContent(request.getAskContent());
        addData.setComment(request.getComment());
        addData.setPublicType(request.getPublicType());
        addData.setAskPassword(request.getAskPassword());

        faqRepository.save(addData);
    }

    public List<FaqAdminItem> getFaqsAdmin(){
        List<Faq> originList = faqRepository.findAll();

        List<FaqAdminItem> result = new LinkedList<>();

        for (Faq faq: originList){
            FaqAdminItem addItem = new FaqAdminItem();
            addItem.setId(faq.getId());
            addItem.setMemberId(faq.getMember().getId());
            addItem.setMemberName(faq.getMember().getUserName());
            addItem.setAskCreateDate(faq.getAskCreateDate());
            addItem.setAskContent(faq.getAskContent());
            addItem.setPublicType(faq.getPublicType() ? "비공개":"공개");
            addItem.setComment(faq.getComment());

            result.add(addItem);
        }
        return result;
    }

    public List<FaqAppItem> getFaqsApp(){
        List<Faq> originList = faqRepository.findAll();

        List<FaqAppItem> result = new LinkedList<>();

        for (Faq faq: originList){
            FaqAppItem addItem = new FaqAppItem();
            addItem.setId(faq.getId());
            addItem.setMemberName(faq.getMember().getUserName());
            addItem.setAskContent(faq.getAskContent());
            addItem.setPublicType(faq.getPublicType() ? "비공개":"공개");

            result.add(addItem);
        }
        return result;
    }

    public FaqResponse getFaq(long id){
        Faq originData = faqRepository.findById(id).orElseThrow();

        FaqResponse response = new FaqResponse();
        response.setId(originData.getId());
        response.setMemberName(originData.getMember().getUserName());
        response.setAskCreateDate(originData.getAskCreateDate());
        response.setAskContent(originData.getAskContent());
        response.setPublicType(originData.getPublicType() ? "비공개":"공개");
        response.setComment(originData.getComment());
        response.setAskPassword(originData.getAskPassword());

        return response;
    }

    public void putFaqAdmin(long id, FaqAdminChangeRequest request){
        Faq originData = faqRepository.findById(id).orElseThrow();

        originData.setComment(request.getComment());

        faqRepository.save(originData);
    }

    public void putFaqUser(long id, FaqUserChangeRequest request){
        Faq originData = faqRepository.findById(id).orElseThrow();

        originData.setAskCreateDate(LocalDate.now());
        originData.setAskContent(originData.getAskContent());
        originData.setPublicType(originData.getPublicType());
        originData.setAskPassword(originData.getAskPassword());

        faqRepository.save(originData);
    }

    public void delFaq (long id){
        faqRepository.deleteById(id);
    }
}
