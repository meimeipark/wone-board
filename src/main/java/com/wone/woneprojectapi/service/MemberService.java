package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.member.MemberCreateRequest;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setUserId(request.getUserId());
        addData.setUserName(request.getUserName());
        addData.setBirthDate(request.getBirthDate());
        addData.setEMail(request.getEMail());
        addData.setPassword(request.getPassword());
        addData.setIsReceive(request.getIsReceive());
        addData.setJoinDate(LocalDate.now());
        addData.setMemberGroup(request.getMemberGroup());
        addData.setMemberStatus(request.getMemberStatus());
        addData.setOutDate(request.getOutDate());

        memberRepository.save(addData);
    }
}
