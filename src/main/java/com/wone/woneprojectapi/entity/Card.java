package com.wone.woneprojectapi.entity;

import com.wone.woneprojectapi.enums.CardGroup;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private CardGroup cardGroup;

    @Column(nullable = false, length = 19, unique = true)
    private String cardNumber;

    @Column(nullable = false)
    private LocalDate endDate;

    @Column(nullable = false)
    private Short cvc;

    @Column(length = 50)
    private String etcMemo;
}
