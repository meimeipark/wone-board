package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyType {
    TURN_OVER("이월"),
    SPENDING("지출");

    private final String moneyType;
}
