package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    ADMIN("관리자"),
    USER_MEMBER("사용자");

    private final String partType;
}
