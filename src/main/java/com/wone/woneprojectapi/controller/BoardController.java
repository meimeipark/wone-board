package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.enums.BoardType;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.service.BoardService;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public String setBoard (@PathVariable long memberId, @RequestBody BoardCreateRequest request){
        Member member = memberService.getData(memberId);
        boardService.setBoard(member, request);

        return "등록되었습니다.";
    }

    @GetMapping("/notice/list")
    public List<BoardNoticeItem> getNoticeBoards (){
        return boardService.getNoticeBoards();
    }

    @GetMapping("/use/list")
    public List<BoardUseItem> getUseBoards (){
        return boardService.getUseBoards();
    }

    @GetMapping("/detail/notice/{id}")
    public BoardNoticeResponse getNoticeBoard(@PathVariable long id){
        return boardService.getNoticeBoard(id);
    }

    @GetMapping("/detail/use/{id}")
    public BoardUseResponse getUesBoard(@PathVariable long id){
        return boardService.getUseBoard(id);
    }

    @PutMapping("/change-content/{id}")
    public String putBoard(@PathVariable long id, @RequestBody BoardContentChangeRequest request){
        boardService.putBoard(id, request);

        return "수정되었습니다.";
    }

    @DeleteMapping("/{id}")
    public String delBoard(@PathVariable long id){
        boardService.delBoard(id);

        return "삭제되었습니다.";
    }
}
