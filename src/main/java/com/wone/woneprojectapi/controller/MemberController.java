package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.model.member.MemberCreateRequest;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember (@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "OK";
    }
}
