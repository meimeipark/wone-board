package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.board.faq.*;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.service.FaqService;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/faq")
public class FaqController {
    private final MemberService memberService;
    private final FaqService faqService;

    @PostMapping("/new/member-id/{memberId}")
    public String setFaq (@PathVariable long memberId, @RequestBody FaqCreateRequest request){
        Member member = memberService.getData(memberId);
        faqService.setFaq(member, request);

        return "OK";
    }

    @GetMapping("/list/admin")
    public List<FaqAdminItem> getFaqsAdmin (){
        return faqService.getFaqsAdmin();
    }

    @GetMapping("/list/app")
    public List<FaqAppItem> getFaqsApp (){
        return faqService.getFaqsApp();
    }

    @GetMapping("/detail/{id}")
    public FaqResponse getFaq(@PathVariable long id){
        return faqService.getFaq(id);
    }

    @PutMapping("/comment/{id}")
    public String putFaqAdmin(@PathVariable long id, @RequestBody FaqAdminChangeRequest request){
        faqService.putFaqAdmin(id, request);

        return "댓글이 등록되었습니다.";
    }

    @PutMapping("/change-faq/{id}")
    public String putFaqUser(@PathVariable long id, @RequestBody FaqUserChangeRequest request ){
        faqService.putFaqUser(id, request);

        return "문의 내용이 수정되었습니다.";
    }

    @DeleteMapping("/{id}")
    public String delFaq(@PathVariable long id){
        faqService.delFaq(id);

        return "삭제되었습니다.";
    }
}
