package com.wone.woneprojectapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "wone App",
                description = "wone app api명세",
                version = "v1")
)
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi woneApi(){
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group(" Wone API v1")
                .pathsToMatch(paths)
                .build();
    }
}
